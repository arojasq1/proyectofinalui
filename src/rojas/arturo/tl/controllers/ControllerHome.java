package rojas.arturo.tl.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.scene.media.Media;
import javafx.scene.control.TextField;
import rojas.arturo.bl.entities.song.Song;
import rojas.arturo.bl.logic.Gestor;

import java.io.File;
import java.time.LocalDate;

public class ControllerHome {

    String path;

    Gestor g = new Gestor();

    Media media;

    MediaPlayer mediaPlayer;

    @FXML
    private Pane popupbox;

    @FXML
    private Button btnCancion;

    @FXML
    private TextField name;

    @FXML
    private TextField artist;

    @FXML
    private TextField genre;

    @FXML
    private TextField composer;

    @FXML
    private TextField album;

    @FXML
    private Button btnSubir;

    @FXML
    private Pane inicio;

    @FXML
    private Pane subir;

    @FXML
    private Pane cancion;

    @FXML
    private Pane biblioteca;

    @FXML
    private Text songName;

    @FXML
    private Pane btnPlay;


    @FXML
    void hidePopUp(MouseEvent event) {
        popupbox.setVisible(false);
    }

    @FXML
    void showPopUp(MouseEvent event) {
        popupbox.setVisible(true);
        btnCancion.setVisible(false);
        name.setVisible(false);
        artist.setVisible(false);
        album.setVisible(false);
        genre.setVisible(false);
        composer.setVisible(false);
        btnSubir.setVisible(false);
    }

    @FXML
    void showSongUp(MouseEvent event) {
        btnCancion.setVisible(true);
        name.setVisible(true);
        artist.setVisible(true);
        album.setVisible(true);
        genre.setVisible(true);
        composer.setVisible(true);
        btnSubir.setVisible(true);
    }

    @FXML
    void subirC(ActionEvent event) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Canción");
            fileChooser.setInitialDirectory(new File("/home/quija/IdeaProjects/proyectofinalui/src/rojas/arturo/ui/recursos/mp3"));
            File mp3File = fileChooser.showOpenDialog(null);

            if (mp3File != null) {
                path="/home/quija/IdeaProjects/proyectofinalui/src/rojas/arturo/ui/recursos/mp3/"+mp3File.getName();
                btnCancion.setText("Listo!");
            };
    }

    @FXML
    void subirCBD(ActionEvent event) throws Exception {
        g.registrarSong2(name.getText(),LocalDate.parse("2003-08-21"),5.0,1000.0,path,"1",composer.getText(),
                genre.getText(),album.getText());
        btnCancion.setVisible(false);
        name.setVisible(false);
        artist.setVisible(false);
        album.setVisible(false);
        genre.setVisible(false);
        composer.setVisible(false);
        btnSubir.setVisible(false);
    }

    @FXML
    void showSongs(MouseEvent event) throws Exception {
        Song cancion = g.getCancion();
        String nombre = cancion.getName();
        songName.setText(nombre);
    }


    @FXML
    void playSong(MouseEvent event) throws Exception{
        System.out.println("play");
        Song cancion = g.getCancion();
        String url = cancion.getPath();

        //could not create media player?
        media = new Media(new File(url).toURI().toString());
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setAutoPlay(true);
    }
}

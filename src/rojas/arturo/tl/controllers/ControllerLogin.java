package rojas.arturo.tl.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import rojas.arturo.bl.logic.Gestor;

import java.util.ArrayList;

public class ControllerLogin {

    @FXML
    private TextField userField;

    @FXML
    private PasswordField passField;

    @FXML
    private Pane loginBoton;

    @FXML
    private Label mensaje;

    @FXML
    void login(MouseEvent event) throws Exception {
       Gestor g = new Gestor();
       boolean login;
       login = g.login(userField.getText().toLowerCase(),passField.getText().toLowerCase());
       if(login){
                new ToScene().toScene("Home.fxml", event);
                mensaje.setText("");
            } else {
                mensaje.setText("Correo o contraseña incorrecta");
            }
        }

    @FXML
    void showField(MouseEvent event) {
        userField.getStyleClass().remove("hidden");
        passField.getStyleClass().remove("hidden");
    }

}
